#!/bin/sh

echo  ---------------------------------------------------------------------
echo  ------------- Add new modifications on images or config -------------
echo  ---------------------------------------------------------------------
git add ../src/assets/*
git add ../src/config/*


read -p 'Enter your gitlab email: ' email
read -p 'Enter your name: ' name

git config --global user.email $email
git config --global user.name $name

echo  ---------------------------------------------------------------------
echo  ---------------------- Name the new modifications -------------------
echo  ---------------------------------------------------------------------
# git -c user.name='admin' -c user.email='admin@gmail.com' commit -m "Update with script config or images"  
git commit -m "Update with script config or images"  

echo  ---------------------------------------------------------------------
echo  -------- Pull modifications of the remote gitlab repository ---------
echo  ---------------------------------------------------------------------
git pull

echo  ---------------------------------------------------------------------
echo  ---------------------------- Push to server -------------------------
echo  ---------------------------------------------------------------------
git push


echo  ---------------------------------------------------------------------
echo  --------------- Remote gitlab repository is updated -----------------
echo  ---------------------------- COMPLETED ------------------------------
echo  ---------------------------------------------------------------------