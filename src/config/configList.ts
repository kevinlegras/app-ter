export const configListData = {
    GroupementCategoriel : {
        images : {
            familiarisation1 : {
                0 : [
                    "assets/GroupementCategoriel/familiarisation1/fournitureScolaire/ciseaux.jpg",
                    "assets/GroupementCategoriel/familiarisation1/fournitureScolaire/crayon.jpg",
                    "assets/GroupementCategoriel/familiarisation1/fournitureScolaire/livre.jpg",
                    "assets/GroupementCategoriel/familiarisation1/fournitureScolaire/stylo.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/familiarisation1/fruits/ananas.jpg",
                    "assets/GroupementCategoriel/familiarisation1/fruits/banane.jpg",
                    "assets/GroupementCategoriel/familiarisation1/fruits/cerise.jpg",
                    "assets/GroupementCategoriel/familiarisation1/fruits/poire.jpg",
                ],
            },
            familiarisation2 : {
                0 : [
                    "assets/GroupementCategoriel/familiarisation2/fournitureScolaire/ciseaux.jpg",
                    "assets/GroupementCategoriel/familiarisation2/fournitureScolaire/crayon.jpg",
                    "assets/GroupementCategoriel/familiarisation2/fournitureScolaire/livre.jpg",
                    "assets/GroupementCategoriel/familiarisation2/fournitureScolaire/stylo.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/familiarisation2/fruits/ananas.jpg",
                    "assets/GroupementCategoriel/familiarisation2/fruits/banane.jpg",
                    "assets/GroupementCategoriel/familiarisation2/fruits/cerise.jpg",
                    "assets/GroupementCategoriel/familiarisation2/fruits/poire.jpg",
                ],
                2 : [
                    "assets/GroupementCategoriel/familiarisation2/jeux/ballon.jpg",
                    "assets/GroupementCategoriel/familiarisation2/jeux/cartes.jpg",
                    "assets/GroupementCategoriel/familiarisation2/jeux/de.jpg",
                    "assets/GroupementCategoriel/familiarisation2/jeux/poupee.jpg",
                ]
            },
            liste1 : {
                0 : [
                    "assets/GroupementCategoriel/liste1/fournitureScolaire/ciseaux.jpg",
                    "assets/GroupementCategoriel/liste1/fournitureScolaire/crayon.jpg",
                    "assets/GroupementCategoriel/liste1/fournitureScolaire/livre.jpg",
                    "assets/GroupementCategoriel/liste1/fournitureScolaire/stylo.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/liste1/fruits/ananas.jpg",
                    "assets/GroupementCategoriel/liste1/fruits/banane.jpg",
                    "assets/GroupementCategoriel/liste1/fruits/cerise.jpg",
                    "assets/GroupementCategoriel/liste1/fruits/poire.jpg",
                ],
                2 : [
                    "assets/GroupementCategoriel/liste1/jeux/ballon.jpg",
                    "assets/GroupementCategoriel/liste1/jeux/cartes.jpg",
                    "assets/GroupementCategoriel/liste1/jeux/de.jpg",
                    "assets/GroupementCategoriel/liste1/jeux/poupee.jpg",
                ],
                3 : [
                    "assets/GroupementCategoriel/liste1/partieDuCorps/main.jpg",
                    "assets/GroupementCategoriel/liste1/partieDuCorps/nez.jpg",
                    "assets/GroupementCategoriel/liste1/partieDuCorps/oreille.jpg",
                    "assets/GroupementCategoriel/liste1/partieDuCorps/pied.jpg",
                ]
            
            }, 
            liste2 : {
                0 : [
                    "assets/GroupementCategoriel/liste2/animaux/cheval.jpg",
                    "assets/GroupementCategoriel/liste2/animaux/chien.jpg",
                    "assets/GroupementCategoriel/liste2/animaux/cochon.jpg",
                    "assets/GroupementCategoriel/liste2/animaux/lapin.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/liste2/meubles/chaise.jpg",
                    "assets/GroupementCategoriel/liste2/meubles/lit.jpg",
                    "assets/GroupementCategoriel/liste2/meubles/table.jpg",
                    "assets/GroupementCategoriel/liste2/meubles/tabouret.jpg",
                ],
                2 : [
                    "assets/GroupementCategoriel/liste2/outils/balaiexterieur.jpg",
                    "assets/GroupementCategoriel/liste2/outils/marteau.jpg",
                    "assets/GroupementCategoriel/liste2/outils/rateau.jpg",
                    "assets/GroupementCategoriel/liste2/outils/scie.jpg",
                ],
                3 : [
                    "assets/GroupementCategoriel/liste2/vehicules/bus.jpg",
                    "assets/GroupementCategoriel/liste2/vehicules/camion.jpg",
                    "assets/GroupementCategoriel/liste2/vehicules/moto.jpg",
                    "assets/GroupementCategoriel/liste2/vehicules/voiturecommune.jpg",
                ]

            },
            liste3 : {
                0 : [
                    "assets/GroupementCategoriel/liste3/aliments/chocolat.jpg",
                    "assets/GroupementCategoriel/liste3/aliments/fromage.jpg",
                    "assets/GroupementCategoriel/liste3/aliments/gateauauxfruits.jpg",
                    "assets/GroupementCategoriel/liste3/aliments/poulet.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/liste3/instrumentDeMusique/guitare.jpg",
                    "assets/GroupementCategoriel/liste3/instrumentDeMusique/piano.jpg",
                    "assets/GroupementCategoriel/liste3/instrumentDeMusique/tambour.jpg",
                    "assets/GroupementCategoriel/liste3/instrumentDeMusique/trompette.jpg",
                ],
                2 : [
                    "assets/GroupementCategoriel/liste3/petitsAnimaux/coccinelle.jpg",
                    "assets/GroupementCategoriel/liste3/petitsAnimaux/escargot.jpg",
                    "assets/GroupementCategoriel/liste3/petitsAnimaux/fourmi.jpg",
                    "assets/GroupementCategoriel/liste3/petitsAnimaux/papillon.jpg",
                ],
                3 : [
                    "assets/GroupementCategoriel/liste3/vetements/chaussette.jpg",
                    "assets/GroupementCategoriel/liste3/vetements/pantalon.jpg",
                    "assets/GroupementCategoriel/liste3/vetements/pull-over.jpg",
                    "assets/GroupementCategoriel/liste3/vetements/robeenfant.jpg",
                ]
            },
        },
        parametres : {
            imageNumber : 16,
            typeliste : "liste1",
            time : 8
        }
    },
    AutoRepetition : {
        images : {
            familiarisation1:[
                "assets/AutoRepetition/familiarisation1/stylo2.png",
                "assets/AutoRepetition/familiarisation1/zebre2.png",
            ],
            familiarisation2:[
                "assets/AutoRepetition/familiarisation2/os2.png",
                "assets/AutoRepetition/familiarisation2/prise2.png",
                "assets/AutoRepetition/familiarisation2/valise2.png",
            ],
            liste1 : [
                "assets/AutoRepetition/liste1/bol2.png",
                "assets/AutoRepetition/liste1/cartes2.png",
                "assets/AutoRepetition/liste1/crabe.png",
                "assets/AutoRepetition/liste1/porte2.png",
                "assets/AutoRepetition/liste1/rose2.png",
                "assets/AutoRepetition/liste1/sac4.png",
                "assets/AutoRepetition/liste1/sel4.png",
                "assets/AutoRepetition/liste1/velo4.png",
            ], 
            liste2 : [
                "assets/AutoRepetition/liste2/bus2.png",
                "assets/AutoRepetition/liste2/cle2.png",
                "assets/AutoRepetition/liste2/coq2.png",
                "assets/AutoRepetition/liste2/fusee2.png",
                "assets/AutoRepetition/liste2/piano2.png",
                "assets/AutoRepetition/liste2/skis.png",
                "assets/AutoRepetition/liste2/vase2.png",
                "assets/AutoRepetition/liste2/veste2.png",
            ],
            liste3 : [
                "assets/AutoRepetition/liste3/ane.png",
                "assets/AutoRepetition/liste3/arbre2.png",
                "assets/AutoRepetition/liste3/de.png",
                "assets/AutoRepetition/liste3/livre2.png",
                "assets/AutoRepetition/liste3/moto2.png",
                "assets/AutoRepetition/liste3/robe.png",
                "assets/AutoRepetition/liste3/regle.png",
                "assets/AutoRepetition/liste3/table2.png",
            ]
        },
        parametres : {
            familiarisation1 : {
                imageNumber : 2,
                timeDisplay : Infinity,
                timeTempoBetweenCard: 0,
                autoRotate : false,
                randomImg : false,
                autoRestitution : false,
                timeRestitution : 5,
                colorchange : false,
                orderimage : 1
            },
            familiarisation2 : {
                imageNumber : 3,
                timeDisplay : 4,
                timeTempoBetweenCard: 4,
                autoRotate : false,
                randomImg : false,
                autoRestitution : false,
                timeRestitution : 5,
                colorchange : false,
                orderimage : 1
            },
            liste1 : {
                imageNumber : 6,
                timeDisplay : 2,
                timeTempoBetweenCard: 2,
                autoRotate : false,
                randomImg : false,
                autoRestitution : false,
                timeRestitution : 5,
                colorchange : false,
                orderimage : 1
            },
            liste2 : {
                imageNumber : 6,
                timeDisplay : 2,
                timeTempoBetweenCard: 2,
                autoRotate : false,
                randomImg : false,
                autoRestitution : false,
                timeRestitution : 5,
                colorchange : false,
                orderimage : 1
            },
            liste3 : {
                imageNumber : 6,
                timeDisplay : 2,
                timeTempoBetweenCard: 2,
                autoRotate : false,
                randomImg : false,
                autoRestitution : false,
                timeRestitution : 5,
                colorchange : false,
                orderimage : 1
            }

        }
    }
} 