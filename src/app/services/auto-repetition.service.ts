import { Inject, Injectable } from '@angular/core';
import { ParamAutoRepetition } from 'src/app/models/paramAutoRepetition.model';
import { configListData } from 'src/config/configList';
import { configList, CONFIGLIST } from 'src/config/configList.token';

@Injectable({
  providedIn: 'root'
})
export class AutoRepetitionService {

  public paramAutoRepetition!: ParamAutoRepetition;

  constructor( @Inject(CONFIGLIST) private config: configList) {
    this.setDefaultValues('familiarisation1');
  }

  /**
  * Function will reset by default the autoRepetition param from the config
  * @param {keyof typeof configListData.AutoRepetition.images} typeSelected image list selected 
  * @returns {ParamAutoRepetition}
  */
  setDefaultValues(typeSelected : keyof typeof configListData.AutoRepetition.images): ParamAutoRepetition {
    this.paramAutoRepetition = {
      imageNumber: this.config.AutoRepetition.parametres[typeSelected].imageNumber,
      timeDisplay: this.config.AutoRepetition.parametres[typeSelected].timeDisplay,
      timeTempoBetweenCard: this.config.AutoRepetition.parametres[typeSelected].timeTempoBetweenCard,
      autoRotate: this.config.AutoRepetition.parametres[typeSelected].autoRotate,
      randomImg: this.config.AutoRepetition.parametres[typeSelected].randomImg,
      autoRestitution :  this.config.AutoRepetition.parametres[typeSelected].autoRestitution,
      timeRestitution: this.config.AutoRepetition.parametres[typeSelected].timeRestitution,
      typeSelected : typeSelected,
      colorchange : this.config.AutoRepetition.parametres[typeSelected].colorchange,
      orderimage : this.config.AutoRepetition.parametres[typeSelected].orderimage
    };
    return this.paramAutoRepetition;
  }

  /**
  * Function will save the param selected by the user
  * @param {ParamAutoRepetition} param param define by user 
  * @returns {ParamAutoRepetition}
  */
  save(param: ParamAutoRepetition) {
    this.paramAutoRepetition = param;
  }

}