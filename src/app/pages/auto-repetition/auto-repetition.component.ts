import { Component, ChangeDetectionStrategy } from '@angular/core';
import { TYPELIST } from '../../models/auto-repetition.model';

@Component({
  selector: 'app-auto-repetition',
  templateUrl: './auto-repetition.component.html',
  styleUrls: ['./auto-repetition.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutoRepetitionComponent {

  TYPELIST_VALUES = Object.values(TYPELIST);
}
