import { Component, ChangeDetectionStrategy, OnInit, Inject, HostListener, ViewChild, OnDestroy } from '@angular/core';
import { GroupementCategorielService } from '../../../services/groupement-categoriel.service';
import { CdkDragDrop } from "@angular/cdk/drag-drop";
import { Router } from '@angular/router';
import { configList, CONFIGLIST } from 'src/config/configList.token';
import { ModalEscapeComponent } from 'src/app/shared/modal-escape/modal-escape.component';
import Utils from 'src/app/shared/utils/utils';
import Timer from 'src/app/shared/utils/timer';
import RecordScreen from 'src/app/shared/utils/recordScreen';


@Component({
  selector: 'app-experience-groupement-categoriel',
  templateUrl: './experience-groupement-categoriel.component.html',
  styleUrls: ['./experience-groupement-categoriel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExperienceGroupementCategorielComponent implements OnInit, OnDestroy {

  @ViewChild('modalEscape') modalEscape!: ModalEscapeComponent; 

  dataTable: Record<number,Array<string|number>> = {
    8: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 0, "", "", "", "", "", "", "", "", "", 0, 1, 0, "", "", "", "", "", "", "","", 1, 0, 1, "", "", "", "", "", "", "", "", "", 1, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    12: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 0, 1, 2, "", "", "", "", "", "", "", "", 2, 1, 0, "", "", "", "", "", "", "","", 1, 0, 2, "", "", "", "", "", "", "", "", 0, 1, 2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    16: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 5, 5, 5, "", "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", "", 5, 5, 5, "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    20: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    24: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", 5, 5, 5, 5, 5, 5, 5, "", "", "", "", 5, 5, 5, 5, 5, 5, 5, "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
  };

  items!: (String | Number)[];
  style: Record<string,string> = { "width": 160 + "px", "height": 160 + "px" };
  dataSave: Record<string, Array<Record<string, string | number>>> = { "parameters" : [], "movements": [] };
  displayExperience : boolean = false;
  nameParticipant : string = "";
  timer : any;
  record : any;

  constructor(private service: GroupementCategorielService, private router: Router, @Inject(CONFIGLIST) private config: configList) {
  }


  ngOnInit() {    
    this.onResize();
    this.initTable();
    this.initImg();
    this.record = new RecordScreen();
    this.dataSave["parameters"].push({"nb_images" : this.service.paramGroupementCategoriel.imageNumber, "list" : this.service.paramGroupementCategoriel.typeList, "time" : this.service.paramGroupementCategoriel.time.toString().concat(' minutes').replace('.5 minutes',' minutes 30 secondes')})
  }

  ngOnDestroy(): void {
    this.timer.removeTimer();
  }

  /**
  * Function will set the width and height for the img to display by using the screen pixel   
  * @param / 
  * @returns {void}
  */
  onResize() {
    this.style["width"] = Math.round((window.innerWidth - 25) / 11) + "px";
    this.style["height"] = Math.round((window.innerHeight - 25) / 6) + "px";
  }

  /**
  * Function will set the new index of the img when dropping it on the grid 
  * @param {CdkDragDrop<any>} event event on drop item 
  * @returns {void}
  */
  drop(event: CdkDragDrop<any>) {
    this.saveData(event);
    this.items[event.previousContainer.data.index] = event.container.data.item;
    this.items[event.container.data.index] = event.previousContainer.data.item;
  }

  /**
  * Function will init the timer at the beginning of the experience 
  * @param /
  * @returns {void}
  */
  initTimer() {
    this.timer  = new Timer(() => {
      //CLEAR LORS DU NGDESTROY
      Utils.downloadData(this.dataSave,this.nameParticipant);
      this.router.navigate(['/groupementcategoriel']);
    }, this.service.paramGroupementCategoriel.time * 60000);
    this.timer.start();
  }

  /**
  * Function will initiate the img to display randomly for the experience  
  * @param /
  * @returns {void}
  */
  initImg() {
    let arrayImages = JSON.parse(JSON.stringify(this.config.GroupementCategoriel.images[this.service.paramGroupementCategoriel.typeList]));
    for(let i in arrayImages){
      arrayImages[i] = Utils.shuffleArray(arrayImages[i])
    }
    this.items = this.items.map((item: any) => {
      if (item !== "") {
        item = arrayImages[item].shift();
      }
      return item;
    });
  }

  /**
  * Function will select the pattern to use to display the image list  
  * @param /
  * @returns {void}
  */
  initTable(){    
    let nbImages = this.service.paramGroupementCategoriel.imageNumber;
    if (nbImages < 16) {
      this.items = this.dataTable[nbImages];
    }
    else{
      this.items = Utils.createShuffleTabImages(this.dataTable[nbImages],nbImages, 11);
    }
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler() {
    //CHECK SI MODAL OPEN
    if (this.modalEscape.dialog.openDialogs.length == 0){
      this.timer.pause();
      this.modalEscape.openDialog();
    }
  }

  /**
  * Function  will display the experience once the user set a particpantName     
  * @param /
  * @returns {void}
  */
  setDisplayExperience(){
    this.displayExperience = true;
    this.record.setNameParticipant(this.nameParticipant);
    this.initTimer();
  }

  /**
  * Function will save the old/new index of the img when dropping it on the grid
  * @param {CdkDragDrop<any>} event event on drop item 
  * @returns {void}
  */
  saveData(event: CdkDragDrop<any>){
   let newCaseIndex = this.getCase(event.container.data.index);
   let oldCaseIndex = this.getCase(event.previousContainer.data.index);

   let item = this.getFormatItem(this.items[event.previousContainer.data.index]);

    this.dataSave["movements"].push({"oldIndex" : oldCaseIndex, "newIndex":newCaseIndex, "item" : item})
    if(this.items[event.container.data.index] != "" && newCaseIndex != oldCaseIndex){
      let itemSwap = this.getFormatItem(this.items[event.container.data.index]);
      this.dataSave["movements"].push({"oldIndex" : newCaseIndex, "newIndex":oldCaseIndex, "item" : itemSwap})
    }
  }

  /**
  * Function will return the letter and index of new/previous image position on the grid
  * @param {number} val image index
  * @returns {string}
  */  
  getCase(val : number){
    let alphabet : string = 'abcdefghijklmnopqrstuvwxyz';
    let letter : string  = alphabet[Math.trunc(val/11)].toUpperCase();
    let index : number = val % 11 + 1;
    return letter+index;
  }

  /**
  * Function will return path of the current image
  * @param {number} val image index
  * @returns {string}
  */  
  getFormatItem(val : any){
    let liste = this.service.paramGroupementCategoriel.typeList;
    let path = `assets/GroupementCategoriel/${liste}/`;
    return val.replace(path, "").replace(".jpg","");
  }
}