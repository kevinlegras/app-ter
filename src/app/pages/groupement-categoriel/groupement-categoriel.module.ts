import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { GroupementCategorielComponent } from './groupement-categoriel.component';
import { ExperienceGroupementCategorielComponent } from './experience-groupement-categoriel/experience-groupement-categoriel.component';
import { ParametreGroupementCategorielComponent } from './parametre-groupement-categoriel/parametre-groupement-categoriel.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes = [
    { path: '', component: GroupementCategorielComponent },
    { path: "parametre", component: ParametreGroupementCategorielComponent },
    { path: "experience", component: ExperienceGroupementCategorielComponent },
];

@NgModule({
    declarations: [
        GroupementCategorielComponent,
        ExperienceGroupementCategorielComponent,
        ParametreGroupementCategorielComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatButtonModule,
        FormsModule,
        DragDropModule,
        SharedModule
    ],
})
export class GroupementCategorielModule { }
