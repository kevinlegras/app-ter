export default class Timer {
  callback : any;
  remainingTime : any;
  startTime : any;
  timerId : any;
  
  constructor(callback : any, delay :any) {
    this.callback = callback
    this.remainingTime = delay
  }

  /**
  * Function will pause the timer 
  * @param / 
  * @returns {void}
  */
  pause() : void {
    clearTimeout(this.timerId)
    this.remainingTime -= new Date().getTime() - this.startTime
  }
  
  /**
  * Function will start the timer 
  * @param / 
  * @returns {void}
  */
  start() : void {
    this.startTime = new Date().getTime();
    this.timerId = setTimeout(this.callback, this.remainingTime)
  }

  /**
  * Function will remove the timer 
  * @param / 
  * @returns {void}
  */
  removeTimer() : void {
    clearTimeout(this.timerId);
  }

}
  