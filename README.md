# AppTer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.

## Install & launch



```bash
  #Clone the project
  git clone https://gitlab.com/kevinlegras/app-ter.git
```

### Script 

```bash
  #Go to the repository
  cd app-ter/scripts
```

```bash
  #Run the script
  ./installAndLaunch.sh
```
Navigate to `http://localhost:4200/`.

### Commands lines


```bash
  #Go to the repository
  cd app-ter
```

```bash
  #Install dependancies
  npm ci
```

```bash
  #Run the local project
  npm run start
```

Navigate to `http://localhost:4200/`.
